#!/bin/bash

mkdir -p /opt/ops/puppet

# Unzip puppet configuration archive
tar -xvf /opt/ops/puppet.tar.gz -C /opt/ops/puppet

puppet apply /opt/ops/puppet/setup.pp --modulepath=/opt/ops/puppet/modules
