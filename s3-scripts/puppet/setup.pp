
class { 'apache':
        default_mods  => false,
        default_vhost => false
}

# For rewriting HTTP URLs to HTTPS
# apache::mod { 'rewrite': }

# For the DirectoryIndex directive
apache::mod { 'dir': }

apache::vhost { 'streamco':
        port            => 80,
        docroot         => '/var/www/html',
        serveradmin     => 'james.ducker@gmail.com',
        docroot_owner   => 'www-data',
        docroot_group   => 'www-data',
        error_documents => [
                { 'error_code' => '404', 'document' => '/404.html' }
        ],
        rewrites       => [
                {
                        # This will 301 redirect HTTP requests that have been forwarded from the ELB
                        # to HTTPS URLs
                        rewrite_cond => [ '%{HTTP:X-Forwarded-Proto} =http' ],
                        rewrite_rule => [ '. https://%{HTTP:Host}%{REQUEST_URI} [L,R=permanent]' ]
                }
        ],
        directories     => [
                {
                        path           => '/var/www/html',
                        directoryindex => 'instance.html',
                        options        => [ 'FollowSymLinks' ]
                }
        ]
}

file { '/var/www/html/404.html':
        ensure  => file,
        owner   => 'www-data',
        group   => 'www-data',
        source  => 'puppet:///modules/setup/404.html',
        mode    => '0644'
}

exec { 'create-index-document':
        path    => [ '/usr/bin', '/bin' ],
        command => 'echo "<h1>I am $(curl http://169.254.169.254/latest/meta-data/instance-id)</h1>" > /var/www/html/instance.html',
        creates => '/var/www/html/instance.html',
        require => Class['apache']
}
