# StreamCo solution

## Configuration

### s3-scripts/puppet/puppet.tar.gz

1. r10k was used to resolve dependencies in the Puppetfile, e.g.
   `r10k puppetfile install`. This generates a modules/ directory

2. A setup directory was added under modules, containing the 404.html file source

## Architecture overview

This solution uses Cloudformation to create a load-balanced Ubuntu 16.04 LTS environment using ELB and EC2. The solution
exists inside a VPC with two public subnets. Access to the back-end instances is restricted by security group ingress rules.

Cloudformation uses cfn-init and cloud-init to deploy some setup scripts from an S3 bucket and install puppet from apt repositories.
cloud-init then executes the setup script in each machine which prepares the Puppet configuration and calls puppet.

Puppet is then responsible for installing and configuring apache and running the apache service.

The Cloudformation configuration then mostly contains boilerplate necessary for creating and securing the VPC environment,
including security groups, subnets, route tables, and so on.

In a production environment, rather than having everything in public subnets I would have the load balancer sitting across the public
subnets, and move the actual application back-end into private subnets.

## How would I further automate the management of the infrastructure

There's plenty of room to further automate this solution.

1. I would introduce a centralised build and deploy system such as Jenkins to manage compilation
of the Puppet configuration and its dependencies, and pushing that to the ops S3 bucket.
2. I'd move the application into its own repository and allow it to be built and deployed as a separate entity
   to the base server configuration. This would allow faster deployment of code changes without the need to
   replace instances (as the current solution only runs configuration on instance launch).
2. I would write autoscaling rules into the Cloudformation template to allow the application to scale
with load.
3. I would introduce a monitoring system such as Nagios, as well as other systems to support this, such as
using `collectd` to provide more detailed system health metrics than what EC2/ELB healthchecks can provide
4. Pushing automation even further, I would create a deployment process in Jenkins to build and pre-configure an AMI
   to be used as the base application server configuration.
5. Now that the system is scalable and well-monitored, introducing robust unit testing and regression testing of the 
application code would allow the creation of a true CI/CD pipeline, with deployment becoming totally hands-off.
6. To improve deployment and rollback times, I might then go even further and create a green/blue deployment architecture
   using a system such as Netflix's _Spinnaker_. In a mature application environment, this would be a challenging change,
   as it requires quite a lot of architectural buy-in to make it work well.
